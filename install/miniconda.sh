#! /bin/sh

set -euo pipefail
set -x

rm -f Miniconda3-latest-Linux-x86_64.sh
URL=https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
PREFIX='/opt/miniconda3'
wget --no-verbose $URL
sh ${URL##*/} -b -p $PREFIX
rm -f ${URL##*/}
$PREFIX/bin/conda install -y --copy git
$PREFIX/bin/pip install csvkit
# ln -s $PREFIX/bin/conda /usr/local/bin/
# ln -s $PREFIX/bin/activate /usr/local/bin/
# ln -s $PREFIX/bin/deactivate /usr/local/bin/
# ln -s $PREFIX/bin/pip /usr/local/bin/

# usage: $0 [options]
# Installs Miniconda3 4.3.30
# -b           run install in batch mode (without manual intervention),
#              it is expected the license terms are agreed upon
# -f           no error if install prefix already exists
# -h           print this help message and exit
# -p PREFIX    install prefix, defaults to $PREFIX, must not contain spaces.
# -s           skip running pre/post-link/install scripts
# -u           update an existing installation
# -t           run package tests after installation (may install conda-build)
