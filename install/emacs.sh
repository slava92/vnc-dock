#! /bin/bash

set -euo pipefail
version=26.1

wget -nc --no-verbose http://git.savannah.gnu.org/cgit/emacs.git/snapshot/emacs-$version.tar.gz
tar xzvf emacs-$version.tar.gz
cd emacs-$version
./autogen.sh

./configure --without-makeinfo --with-x-toolkit=gtk --with-xpm=yes --with-jpeg=yes --with-png=yes --with-gif=yes --with-tiff=yes --with-rsvg=yes --prefix=/opt/emacs-$version

sudo make install
cd ..
sudo rm -fr emacs-$version*
