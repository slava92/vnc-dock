#! /bin/bash

set -euo pipefail

CONDA=/opt/miniconda3/bin/conda
# Create Python 2 environment for supervisord
SUP_ENVIRONMENT=/headless/supervisord
echo >&2 "Updating supervisor conda environment at $SUP_ENVIRONMENT"
mkdir -p $SUP_ENVIRONMENT
echo "Y" | $CONDA install -p $SUP_ENVIRONMENT -m --yes --copy python=2 pip
$SUP_ENVIRONMENT/bin/pip install supervisor
mkdir -p $SUP_ENVIRONMENT/logs $SUP_ENVIRONMENT/conf
cp -p supervisord.conf $SUP_ENVIRONMENT/conf/
