#! /bin/bash

set -euo pipefail

wget --no-verbose https://github.com/source-foundry/Hack/releases/download/v3.000/Hack-v3.000-ttf.zip
unzip Hack-v3.000-ttf.zip -d /headless/.fonts
fc-cache /headless/.fonts
rm -f Hack-v3.000-ttf.zip
