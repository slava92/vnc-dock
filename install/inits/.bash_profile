#
# ~/.bash_profile
#

(( SHLVL == 1 )) && {
    if [[ -z ${VIRGIN_PATH:-} ]]; then
        export PATH=~/bin:~/miniconda3/bin:~/.local/bin:$PATH
        export VIRGIN_PATH=$PATH
        export LIBRARY_PATH=/opt/X11/lib:${LIBRARY_PATH:-}
        export CPATH=/opt/X11/include:${CPATH:-}
    fi
}
export SVN_EDITOR=vi
export BOOT_JVM_OPTIONS='-Xmx2g -client -XX:+TieredCompilation -XX:TieredStopAtLevel=1 -Xverify:none'
export BOOT_COLOR=off
export BOOT_CLOJURE_VERSION=1.8.0
export HOMEBREW_GITHUB_API_TOKEN="aefa39df3c7b9c8a8c99471b6ca8d5f59063d2af"

rm -f ~/Chuma/*-ips.txt || : ignore
ip route > ~/Chuma/$(hostname -f)-ips.txt || : ignore too

[[ -f ~/.bashrc ]] && . ~/.bashrc
