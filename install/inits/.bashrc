#! /bin/bash
#
# ~/.bashrc
#

if [[ -z ${NSS_WRAPPER_PASSWD:-} ]]; then
    source /headless/.bashrc.orig
    /headless/supervisord/bin/supervisord -c /headless/supervisord/conf/supervisord.conf
fi

(( SHLVL == 1 )) && {
    export PATH=~/bin/:~/.local/bin:/opt/miniconda3/bin:$PATH
    export VIRGIN_PATH=$PATH
}

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
umask 0022
if ssh-add -l | grep -q SHA256; then
    : ok, no need
else
    ls $HOME/.ssh/*.pub | grep -Ev 'old-' | sed 's/.pub$//' | xargs ssh-add
fi
_MYSELF=$(id -nu)
PS1='$? vnc:\w > '
HOME=/headless

[[ ${TERM:-undefined} == 'dumb' ]] && PS1='$ '

set -o emacs
unset TMOUT
export LESS=-icsb40m
export _STTY=$(stty -g)
stty -g > ~/.stty.save

alias a='alias'
alias d='dirs'
alias d2='dirs +1'
alias df='/bin/df -k'
alias dir='/bin/ls -FaC'
alias dxpc='. ~/lib/dxpc.sh'
alias et='echo -e '\''\033];'\'''
alias grep='grep --color=auto'
alias h='history +22'
alias hh='history'
alias j='jobs -l'
alias jboss='sudo /bin/su - jboss'
alias l='/bin/ls -C'
alias l.='ls -d .* --color=auto'
alias lc='/bin/ls -FC'
alias lf='/bin/ls -FaC'
alias ll='/bin/ls -la'
alias lld='/bin/ls -lad'
alias ls='ls --color=auto'
alias m='less'
alias more='less'
alias netinet='netstat -f inet'
alias nocore='cp /dev/null core && chmod 0 core'
alias p='popd'
alias pd='pushd'
alias psef="ps -ef"
alias psfu="ps -fu $_MYSELF"
alias rm='/bin/rm -i'
alias s='suspend'
alias sane='stty $_STTY'
alias which='alias | /usr/bin/which --tty-only --read-alias --show-dot --show-tilde'
alias z='suspend'
alias unpath='PATH=$VIRGIN_PATH'
alias ema='EDITOR=emacsclient gox emacs -g 100x42'
alias sshy='ssh -Y'
