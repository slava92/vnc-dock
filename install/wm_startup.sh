#!/usr/bin/env bash

set -euo pipefail

### disable screensaver and power management
xset -dpms &
xset s noblank &
xset s off &
xsetroot -cursor_name left_ptr
# set up keyboard layout
setxkbmap -layout us -option grp:alt_caps_toggle,grp_led:scroll,caps:none
# setxkbmap -option caps:none
[[ -f ~/.Xmodmap ]] && xmodmap ~/.Xmodmap

LOGS=/private/CENTOS/headless
[[ -d $LOGS ]] || LOGS=$HOME
echo > $LOGS/wm.log

echo Starting xfce4
/usr/bin/startxfce4 --replace > $HOME/wm.log &
sleep 1
cat $LOGS/wm.log
