## Custom Dockerfile
FROM slava92/centos-xfce-vnc:latest
MAINTAINER Slava Andreyev "slava92@yahoo.com"
ENV REFRESHED_AT 2018-10-23

ARG MY_UID
ARG MY_GID
ARG USER

## Install packages
USER 0
RUN yum update -y
RUN yum group install -y "Development Tools"
RUN yum install -y \
        autossh bsdtar bzip2 curl fuse-sshfs giflib giflib-devel \
        gnutls-devel gtk2-devel gtk3 gtk3-devel imake java-1.8.0-openjdk-devel \
        jq less libX11-devel libXpm libXpm-devel libjpeg-turbo-devel \
        liblockfile libotf libotf-devel librsvg2 librsvg2-devel libtiff-devel \
        libxkbcommon-x11 libxml2-devel lsof make ncurses-devel net-tools \
        openjpeg-devel openjpeg-libs-devel parallel python-psycopg2 ristretto \
        sudo tar unzip wget which zip
# Install desktop related packages
RUN yum install -y \
        xfce4-cpugraph-plugin xfce4-netload-plugin xfce4-notifyd \
        xfce4-xkb-plugin xloadimage xorg-x11-apps xorg-x11-font-utils \
        xorg-x11-fonts-100dpi xorg-x11-fonts-75dpi \
        xorg-x11-fonts-ISO8859-1-100dpi xorg-x11-fonts-ISO8859-1-75dpi \
        xorg-x11-fonts-Type1 xorg-x11-fonts-cyrillic xorg-x11-fonts-misc \
        xorg-x11-utils
# athena: libXaw libXaw-devel (instead of default (gtk))
RUN yum clean all

# set user and time
RUN groupadd -g ${MY_GID} headless
RUN useradd -u ${MY_UID} -g ${MY_GID} -G wheel -m -p wheel -d /headless ${USER}
RUN echo ${USER}:wheel | chpasswd
RUN chown -R ${USER} /headless
RUN rm -f /etc/localtime; ln -s /usr/share/zoneinfo/America/New_York /etc/localtime

RUN mkdir -p /opt
RUN chmod 1777 /opt
ADD ./install/ /headless/install/
WORKDIR /headless/install
RUN sh miniconda.sh
RUN chown -R ${MY_UID} /headless/
RUN cp -p vnc_startup.sh /dockerstartup/.

# Specify the user which should be used to execute all commands below
USER ${USER}
WORKDIR /headless/install
RUN sh supervisord.sh
# Set the working directory to user home directory
WORKDIR /headless
ADD install/inits/.config/xfce4/terminal/terminalrc \
                  .config/xfce4/terminal/terminalrc
ADD install/inits/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml \
                  .config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml
ADD install/hex-wallpapers-012.jpg .config/hex-wallpapers-012.jpg
RUN install/fonts.sh
RUN mv .bashrc .bashrc.orig \
       && mkdir -p bin .logs \
       && cp install/inits/bin/* bin/. \
       && cp install/wm_startup.sh .
RUN for f in .Xclients .Xmodmap .bash_aliases .bash_logout .bash_profile \
             .bashrc .condarc .gitconfig .gitignore .gitignore_global; \
        do cp install/inits/$f $f; done
VOLUME /opt
