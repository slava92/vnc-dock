#! /bin/sh

source ./project.sh
set -euo pipefail

MY_UID=1000 # $(id -u)
MY_GID=1000 # $(id -g)
# DEBUG=--debug
# FROM_SCRATCH='--no-cache'

docker ${DEBUG:-} build \
       --tag $HUB_NAME \
       --build-arg MY_UID="$MY_UID" \
       --build-arg MY_GID="$MY_GID" \
       --build-arg USER="$USER" \
       ${FROM_SCRATCH:-} .
