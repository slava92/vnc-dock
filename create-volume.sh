#! /bin/bash

source ./project.sh
set -euo pipefail

if docker volume inspect $VOLUME_NAME; then
    : nice
else
    docker volume create $VOLUME_NAME
fi

docker volume ls
